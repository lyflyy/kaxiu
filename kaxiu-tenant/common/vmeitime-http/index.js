import http from './interface'

var appid = 'wx0b4c7b7e9defd6e5'

export const baseUrl = http.config.baseUrl
export const baseImg = http.config.baseUrl + '/kaxiu/file?path='

http.interceptor.response = (res) => {
	if(res.data && res.data.status == 401){
		// token过期或未授权
		console.log('token过期或未授权,需要重新进行授权')
		uni.login({
			success(loginRes) {
				http.request({
				    url: '/wx/'+ appid + '/user/login',
				    data: {code: loginRes.code}
				}).then(function(loginValue) {
					uni.setStorageSync('token', loginValue.data.data)
					// 重新获取token成功，跳转首页
					uni.reLaunch({
						url: '../../pages/main/main'
					})
				});
			}
		})
	}
}

//设置请求前拦截器
http.interceptor.request = (config) => {
	config.header = {
		Authorization: uni.getStorageSync('token')
	}
}


// 小程序登陆
export const getSessionKey = (data) => {
	
    return http.request({
        url: '/wx/'+ appid + '/user/login',
        data,
    })
}

// 初始化字典数据
export const getDictionary = () => {

    return http.request({
        url: '/kaxiu/sys-dic',
        method: 'POST', 
        data: ['tip', 'amapKey', 'grabSingle', 'newOrdersVoice'],
		// handle:true
    })
}

// 初始化服务类别
export const getCategories = () => {
    return http.request({
        url: '/kaxiu/service-category',
        method: 'GET', 
    })
}

export const getUserInfo = (data) => {
    return http.request({
        url: '/wx/'+ appid + '/user/info',
        method: 'GET', 
		data,
    })
}

// 获取验证码
export const getVerfication = (data) => {
    return http.request({
        url: '/kaxiu/code',
        method: 'GET', 
		data,
    })
}

// 绑定用户手机号
export const bindUser = (data) => {
    return http.request({
        url: '/kaxiu/basic-user/bind-user',
        method: 'GET', 
		data,
    })
}

// 认证
export const attestation = (data) => {
    return http.request({
        url: '/kaxiu/attestation',
        method: 'POST', 
		data,
    })
}

// 获取有效订单
export const effictiveOrders = (data) => {
    return http.request({
        url: '/kaxiu/server/orders',
        method: 'GET', 
		data,
    })
}

// 下单
export const takeOrder = (data) => {
    return http.request({
        url: '/kaxiu/server/orders',
        method: 'POST', 
		data,
    })
}

// 查询用户审核状态
export const getUserAuditStatus = () => {
    return http.request({
        url: '/kaxiu/attestation',
        method: 'GET'
    })
}

// 获取正在进行中的订单
export const getUnderwayOrder = () => {
    return http.request({
        url: '/kaxiu/server/orders/underway',
        method: 'GET', 
    })
}

// 获取订单列表
export const getOrders = (data) => {
    return http.request({
        url: '/kaxiu/server/orders/my',
        method: 'GET', 
		data,
    })
}

// 结束订单
export const overOrder = (data) => {
    return http.request({
        url: '/kaxiu/server/orders/over',
        method: 'POST', 
		data,
    })
}

// 结束订单
export const getAccount = () => {
    return http.request({
        url: '/kaxiu/server/account',
        method: 'GET', 
    })
}

// 默认全部导出  import api from '@/common/vmeitime-http/'
export default {
	baseUrl,baseImg,
	getSessionKey,
    getDictionary,
	getCategories,
	getUserInfo,
	getVerfication,
	bindUser,
	attestation,
	effictiveOrders,
	getUserAuditStatus,
	takeOrder,
	getUnderwayOrder,
	getOrders,
	overOrder,
	getAccount
}