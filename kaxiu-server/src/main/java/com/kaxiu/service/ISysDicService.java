package com.kaxiu.service;

import com.kaxiu.persistent.entity.SysDic;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ISysDicService extends IService<SysDic> {

    List<SysDic> getDicByArr(String[] dics);

}
