package com.kaxiu.service;

import com.kaxiu.persistent.entity.AccountLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资金账户记录表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IAccountLogService extends IService<AccountLog> {

    /**
     * 根据用户的账户id与交易id查询
     * @param transactionId
     * @param accountId
     * @return
     */
    AccountLog getById(String transactionId, Long accountId);

}
