package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.ServiceCategory;
import com.kaxiu.persistent.mapper.ServiceCategoryMapper;
import com.kaxiu.service.IServiceCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 维修类别(商品)表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class ServiceCategoryServiceImpl extends ServiceImpl<ServiceCategoryMapper, ServiceCategory> implements IServiceCategoryService {

    @Autowired
    private ServiceCategoryMapper categoryMapper;

    @Override
    public List<ServiceCategory> getAllCategory() {
        return categoryMapper.selectAllCategory();
    }

}
