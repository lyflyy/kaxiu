package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.SysRoleUser;
import com.kaxiu.persistent.mapper.SysRoleUserMapper;
import com.kaxiu.service.ISysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色中间表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements ISysRoleUserService {

}
