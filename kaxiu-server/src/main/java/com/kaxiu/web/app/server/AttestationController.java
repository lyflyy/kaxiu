package com.kaxiu.web.app.server;


import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.service.IAttestationService;
import com.kaxiu.vo.AttestationVo;
import com.kaxiu.vo.ResultDataWrap;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 认证记录 前端控制器
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@RestController
@RequestMapping("/kaxiu/attestation")
public class AttestationController extends AbstractBaseController {

    @Resource
    private IAttestationService service;

    @Resource
    private RedisService redisService;

    @PostMapping(value = "")
    @RequiresRoles("kaxiu-service")
    public ResultDataWrap attestation(@RequestBody AttestationVo vo) {
        String existCode = redisService.getMobileCode(vo.getMobile());
        // 验证验证码成功，绑定用户以及手机号
        if(vo.getCode().equals(existCode)){
            return buildResult(service.saveAttestation(vo));
        }
        return buildFailResult("验证码错误！");
    }


    @GetMapping(value = "")
    @RequiresRoles("kaxiu-service")
    public ResultDataWrap attestation() {
        BasicUser user = redisService.getWxUser();
        return buildResult(user.getAuditStatus());
    }

}

