package com.kaxiu.web.app.consumer;

import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.service.IBasicUserService;
import com.kaxiu.vo.ResultDataWrap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: LiYang
 * @create: 2019-08-16 00:41
 * @Description:
 **/
@RestController
@RequestMapping("/kaxiu/basic-user")
public class ConsumerUserController extends AbstractBaseController {

    @Resource
    private IBasicUserService userService;

    @Resource
    private RedisService redisService;

    /**
     * consumer小程序使用接口
     * @param mobile
     * @param code
     * @return
     */
    @RequestMapping("/bind-user")
    public ResultDataWrap bindUser(@RequestParam String mobile
            , @RequestParam String code){
        String existCode = redisService.getMobileCode(mobile);
        // 验证验证码成功，绑定用户以及手机号
        if(code.equals(existCode)){
            return buildResult(userService.bindMobile(mobile));
        }
        return buildFailResult("验证码错误！");
    }


}
