package com.kaxiu.web;

//import com.kaxiu.config.artemis.Producer;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.R;
import com.kaxiu.config.CommonConstant;
import com.kaxiu.config.mqtt.MqttSenderConfig;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.config.spring.annotation.ServerValidation;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.persistent.entity.OrderItem;
import com.kaxiu.persistent.entity.Orders;
import com.kaxiu.persistent.entity.RepairOrder;
import com.kaxiu.persistent.mapper.OrdersMapper;
import com.kaxiu.persistent.mapper.RepairOrderMapper;
import com.kaxiu.service.IBasicUserService;
import com.kaxiu.service.IOrdersService;
import com.kaxiu.util.LocationUtil;
import com.kaxiu.vo.enums.AttestationEnum;
import com.kaxiu.vo.order.OrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static com.kaxiu.config.CommonConstant.OrderStatus.ORDER_STATUS_PAYED_WATTING;
import static com.kaxiu.config.CommonConstant.PayStatus.PAY_STATUS_PAYED;
import static com.kaxiu.config.CommonConstant.PayStatus.PAY_STATUS_UN_PAY;


/**
 * @author: LiYang
 * @create: 2019-08-10 01:24
 * @Description:
 **/
@RequestMapping("")
@Controller
public class TestController {

    @Autowired
    private MqttSenderConfig.MyGateway mqttGateway;

    @Autowired
    private IBasicUserService basicUserService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private RepairOrderMapper repairOrderMapper;

    @Autowired
    private IOrdersService ordersService;

    @RequestMapping("/sendMqtt")
    @ResponseBody
    public String sendMqtt(String  sendData){
        mqttGateway.sendToMqtt("topic","hello， 反反复复方法可王德法");
        mqttGateway.sendToMqtt("反反复复方法可王德法反反复复方法可王德法反反复复方法可王德法");
        return "OK";
    }

    /**
     * 修改用户状态为已审核
     * 我的userid 为 1162306000523374593
     * 修改数据库，修改redis
     * @return
     */
    @RequestMapping("/1")
    @ResponseBody
    public String test1(){
        BasicUser b = new BasicUser();
        b.setId(1163467986976227330L);
        b.setAuditStatus(AttestationEnum.CERTIFIED);
        basicUserService.updateById(b);

        b = basicUserService.getById(1162306000523374593L);
        redisService.setWxUser(b);
        return "OK";
    }

    private void test2Impl(Long orderId){
        //支付成功调用
        ordersService.paySuccess(orderId);
        //todo - 现在 order 完成后,直接推送，应该在微信支付成功，修改order状态后再推送
        //todo - 推送的topic 需要处理
        //todo - 推送的条件，按照距离筛选
        List<OrderResponseVo> orderResponseVos = ordersMapper.selectEffectiveOrderList(orderId);
        // 按照维修人员当前位置计算订单与之的距离
        //JSONObject locationJson = redisService.getLocation();
        if(orderResponseVos != null && orderResponseVos.size() > 0){
            OrderResponseVo vo = orderResponseVos.get(0);
            //暂时先用固定数据

            JSONObject locationJson = JSONObject.parseObject(location);
            double distance = LocationUtil.distance(
                    locationJson.getDouble("latitude"),locationJson.getDouble("longitude"),
                    vo.getLocation().getDouble("latitude"), vo.getLocation().getDouble("longitude"));
            vo.setDistance(distance);
            mqttGateway.sendToMqtt("topic",
                    JSON.toJSONString(vo));
        }
    }

    String location = "{\"address\":\"北京市东城区东长安街\",\"latitude\":39.908823,\"errMsg\":\"chooseLocation:ok\",\"name\":\"天安门\",\"longitude\":116.39747}";

    /**
     * 支付成功
     * 修改订单状态为已支付，需要输入订单id，需要去数据库中查询想要修改的订单id
     * 已支付完成，需要想mqtt发布一条本订单消息，服务人员可以收到消息
     * orderId 是orders表中的id
     * 后续对接微信支付 就是这么个逻辑
     * 修改2张表的状态
     * @return
     */
    @RequestMapping("/2")
    @ResponseBody
    public String test2(@RequestParam(required = false) Long orderId){
        if(orderId == null){
            Arrays.stream(arr).forEach( e -> this.test2Impl(e));
        }else{
            this.test2Impl(orderId);
        }

        return "OK";
    }

    @RequestMapping("/4")
    @ResponseBody
    public String test4(@RequestParam(required = false) Long orderId){
        //修改repareOrder表的状态与位置和 。。。
        RepairOrder repairUpdate = new RepairOrder();
        //维修人员id
        repairUpdate.setPersonalLocation(location);
        repairUpdate.setStatus(10);
        repairOrderMapper.update(repairUpdate,
                Wrappers.lambdaUpdate(new RepairOrder()).eq(RepairOrder::getOrderId, orderId));

        return "OK";
    }

    Long[] arr = new Long[]{1162286469138653185L,
            1162301944505565185L,
            1162302362883194882L,
            1162302551152918529L,
            1162302569343614978L,
            1162302815071109121L,
            1162303357264592897L,
            1162303493562695681L,
            1162426180851302401L,
            1162431135163105281L,
            1162439548005900289L,
            1162447148554510338L,
            1162447410325217282L};

    /**
     * 对应test2
     * 将已经支付的订单修改为未支付，测试用
     * @param orderId
     * @return
     */
    @RequestMapping("/2/1")
    @ResponseBody
    public String test21(@RequestParam(required = false) Long orderId){
        if(orderId != null){
            this.test21Impl(orderId);
        }else{
            Arrays.stream(arr).forEach( e -> this.test21Impl(e));
        }
        return "OK";
    }

    private void test21Impl(Long orderId){
        Orders orders = new Orders();
        orders.setId(orderId);
        orders.setPayStatus(PAY_STATUS_UN_PAY);//'0:未支付，1:已支付,2:已关闭;3,支付失败',
        orders.setStatus(1);//'状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单',
        ordersMapper.updateById(orders);
        RepairOrder repairOrder = new RepairOrder();
        repairOrder.setStatus(1);//'状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单',
        RepairOrder repairOrder2 = new RepairOrder();
        repairOrder2.setOrderId(orderId);
        repairOrderMapper.update(repairOrder, Wrappers.lambdaUpdate(repairOrder2).eq(RepairOrder::getOrderId, orderId));
    }



}
