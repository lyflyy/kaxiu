package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 资金账户表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface AccountMapper extends BaseMapper<Account> {

    int addAmount(@Param("accountId") Long accountId, @Param("amount") BigDecimal amount);

    int minusAmount(@Param("accountId") Long accountId, @Param("amount") BigDecimal amount);

    int addAmountByUserId(@Param("userId") Long userId, @Param("amount") BigDecimal amount);
}
