package com.kaxiu.persistent.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 维修类别(商品)表
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ServiceCategory extends BaseEntity {

private static final long serialVersionUID=1L;

    @TableField(exist =false)
    private List<ServiceCategory> categoryList;

    /**
     * 父维修类别id
     */
    private Long parentId;

    /**
     * 类别名称
     */
    private String name;

    /**
     * 类别拼音（小程序顶部菜单栏滚动使用）
     */
    private String pinyin;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 描述
     */
    private String descripion;

    /**
     * 价格
     */
    private BigDecimal amount;


}
