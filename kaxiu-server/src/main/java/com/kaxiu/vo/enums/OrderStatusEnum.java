package com.kaxiu.vo.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author: LiYang
 * @create: 2019-08-05 20:15
 * @Description: 状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单
 **/
@Deprecated
public enum OrderStatusEnum  implements IEnum<Integer> {

    NOT_PAY(1, "发起订单，未支付"),
    PAR_WAIT_ORDER(2, "已支付，等待接单"),
    ORDERED(3, "已接单，正在赶往现场"),
    FINISHED_ORDER(4, "订单完成"),
    CANCLE_ORDER(5, "取消订单");

    @EnumValue
    private Integer value;

    @TableField(exist = false)
    private String desc;

    OrderStatusEnum(final Integer value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static OrderStatusEnum parseValue(Integer v) {
        if (v == null) {
            return null;
        }
        for (OrderStatusEnum e : OrderStatusEnum.values()) {
            if (e.getValue().equals(v)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }}
