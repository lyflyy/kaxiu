package com.kaxiu.vo;

import lombok.Data;

/**
 * @author: LiYang
 * @create: 2019-08-12 18:22
 * @Description:
 **/
@Data
public class AttestationVo {

    private String name;
    private String mobile;
    private String code;
    private String idCard;
    private Integer auditType;
    private String imgFrontPath;
    private String imgBackPath;
    private String imgHandheldPath;


}
