package com.kaxiu.vo.order;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: LiYang
 * @create: 2019-08-04 21:42
 * @Description:
 **/
@Data
public class OrderResponseVo {

    /**
     * 小程序所需订单列表内容
     * 维修类别对象
     *      {
     *          类别名称
     *          菜单图标
     *          描述
     *          价格
     *      }
     * 跑腿费（前端已经存在）
     * 实际支付订单付费价格
     * repair_order的备注
     * 订单完成时间
     * orders的状态
     * 维修人员 name
     * 联系人
     * 联系人手机号
     * 现场照片
     * 维修拍摄照片
     * 位置
     * 评价
     * 评分
     */

    private String id;
    private String serviceName;
    private String serviceIcon;
    private String serviceDescripion;
    private String serviceAmount;
    private BigDecimal realAmount;
    private String contact;
    private String mobile;
    private Integer status;
    private String remark;
    private Date createdTime;
    private Date finishedTime;
    private String personalName;
    private String personalMobile;

    private String scenePhoto;
    private String beforeRepairPhoto;
    private String afterRepairPhoto;
    private JSONObject location;
    private JSONObject personalLocation;
    private String review;
    private String starLevel;

    /**
     * 两点位置的距离
     */
    private double distance;


}
