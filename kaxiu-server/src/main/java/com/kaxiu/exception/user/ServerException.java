package com.kaxiu.exception.user;

import com.kaxiu.exception.BizException;
import com.kaxiu.vo.ResultDataMsg;

/**
 * @author: LiYang
 * @create: 2019-08-15 23:38
 * @Description:
 **/
public class ServerException extends BizException {

    public ServerException(ResultDataMsg resultDataMsg) {
        super(resultDataMsg.getMsg());
        this.code = resultDataMsg.getStatus();
        this.msg = resultDataMsg.getMsg();
    }

}
