package com.kaxiu.exception;


import com.kaxiu.vo.ResultDataMsg;

/**
 * 统一事物异常 错误编码从 10050001开始
 *
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2016/11/17  下午9:07
 */
public class DaoException extends BizException {

    // 数据库相关错误编码 10040001
    /**
     * 数据库操作,insert返回0
     */
    public static final BizException DB_INSERT_RESULT_0 = new BizException(10040001, "数据库操作,insert返回0");

    /**
     * 数据库操作,update返回0
     */
    public static final BizException DB_UPDATE_RESULT_0 = new BizException(10040002, "数据库操作,update返回0");

    /**
     * 数据库操作,selectOne返回null
     */
    public static final BizException DB_SELECTONE_IS_NULL = new BizException(10040003, "数据库操作,selectOne返回null");

    /**
     * 数据库操作,list返回null
     */
    public static final BizException DB_LIST_IS_NULL = new BizException(10040004, "数据库操作,list返回null");

    /**
     * 生成序列异常时
     */
    public static final BizException DB_GET_SEQ_NEXT_VALUE_ERROR = new BizException(10040005, "序列生成超时");

    /**
     * 事务异常
     */
    public static final DaoException TRANSCATIONAL_EXCEPTION = new DaoException(10040006, "事务异常");

    /**
     * 唯一键或者主键重复
     */
    public static final DaoException DUPLICATEKEY_EXCEPTION = new DaoException(10040007, "唯一键或者主键重复");

    /**
     * 异常信息
     */
    protected String msg;

    /**
     * 具体异常码
     */
    protected int code;

    public DaoException() {
        super();
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }


    public DaoException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
        this.msg = String.format(msgFormat, args);
    }


    /**
     * 实例化异常
     *
     * @param msgFormat
     * @param args
     * @return
     */
    public DaoException newInstance(String msgFormat, Object... args) {
        return new DaoException(this.code, msgFormat, args);
    }


    public String getMsg() {
        return msg;
    }

    public DaoException setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public int getCode() {
        return code;
    }

    public DaoException setCode(int code) {
        this.code = code;
        return this;
    }

    public ResultDataMsg getErrorMsg() {
        return new ResultDataMsg(this.code, this.msg);
    }
}
