package com.kaxiu.config.spring.aspect;

import com.kaxiu.config.redis.RedisService;
import com.kaxiu.config.spring.annotation.ServerValidation;
import com.kaxiu.exception.user.ServerException;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.vo.ResultDataMsg;
import com.kaxiu.vo.enums.AttestationEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.rowset.serial.SerialException;
import java.util.Map;

/**
 * @author: LiYang
 * @create: 2019-08-15 22:12
 * @Description: 过滤 接单人员的状态
 **/

@Aspect
@Component
public class ServerValidationAspect {

    @Autowired
    private RedisService redisService;


    @Pointcut(value = "@annotation(com.kaxiu.config.spring.annotation.ServerValidation)")
    public void dataFilterCut() {
    }

    @Before(value = "dataFilterCut() && @annotation(serverValidation)")
    public void validFilter(JoinPoint point, ServerValidation serverValidation) throws Throwable {

        if(!validServer(serverValidation.auditStatus(), serverValidation.deleted(),
                serverValidation.status() )){
            throw new ServerException(ResultDataMsg.SERVER_ERROR);
        }
    }

    /**
     * 校验 用户状态
     * @return
     * @param attestationEnum
     * @param deleted
     * @param status
     */
    public Boolean validServer(AttestationEnum attestationEnum, boolean deleted, String status){
        BasicUser user = redisService.getWxUser();
        return user.getAuditStatus() == attestationEnum && user.getStatus() == Integer.valueOf(status)
                && user.getDeleted() == deleted;
    }

}
