package com.kaxiu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kaxiu.util.LocationUtil;
import com.kaxiu.vo.order.OrderResponseVo;
import org.apache.shiro.crypto.hash.Hash;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: LiYang
 * @create: 2019-08-01 21:01
 * @Description:
 **/
public class Test {

    static String str = "{\n" +
            "  \"msg\" : \"success\",\n" +
            "  \"status\" : 200,\n" +
            "  \"data\" : [ {\n" +
            "    \"id\" : \"1162303357264592897\",\n" +
            "    \"serviceName\" : \"维修类型1\",\n" +
            "    \"serviceIcon\" : \"https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132\",\n" +
            "    \"serviceDescripion\" : \"轮胎：100，油钱：20，人工费：33\",\n" +
            "    \"serviceAmount\" : \"120.000000\",\n" +
            "    \"realAmount\" : 211.000000,\n" +
            "    \"contact\" : \"李阳\",\n" +
            "    \"mobile\" : \"12312312312\",\n" +
            "    \"status\" : 2,\n" +
            "    \"remark\" : \"as\",\n" +
            "    \"createdTime\" : \"2019-08-16 18:01:29\",\n" +
            "    \"scenePhoto\" : \"wx5dc5c3f431a47092.o6zAJs8j5u5v560EaSZ1wv-XA7QQ.qZHV16ziyBTU4036ae2a1b5f9e7851e84613813381ff.png\",\n" +
            "    \"location\" : {\n" +
            "      \"address\" : \"北京市东城区东长安街\",\n" +
            "      \"latitude\" : 39.908823,\n" +
            "      \"errMsg\" : \"chooseLocation:ok\",\n" +
            "      \"name\" : \"天安门\",\n" +
            "      \"longitude\" : 116.39747\n" +
            "    },\n" +
            "    \"distance\" : 0.0\n" +
            "  }, {\n" +
            "    \"id\" : \"1162303493562695681\",\n" +
            "    \"serviceName\" : \"维修类型4\",\n" +
            "    \"serviceIcon\" : \"https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132\",\n" +
            "    \"serviceDescripion\" : \"轮胎：400，油钱：20，人工费：33\",\n" +
            "    \"serviceAmount\" : \"630.000000\",\n" +
            "    \"realAmount\" : 721.000000,\n" +
            "    \"contact\" : \"李阳\",\n" +
            "    \"mobile\" : \"12312312312\",\n" +
            "    \"status\" : 2,\n" +
            "    \"remark\" : \"asd\",\n" +
            "    \"createdTime\" : \"2019-08-16 18:02:02\",\n" +
            "    \"scenePhoto\" : \"wx5dc5c3f431a47092.o6zAJs8j5u5v560EaSZ1wv-XA7QQ.1AdFUJ3y5jjC34a012c03747e39b09fe9ae68d745c29.png\",\n" +
            "    \"location\" : {\n" +
            "      \"address\" : \"北京市东城区东长安街\",\n" +
            "      \"latitude\" : 39.90374,\n" +
            "      \"errMsg\" : \"chooseLocation:ok\",\n" +
            "      \"name\" : \"天安门广场\",\n" +
            "      \"longitude\" : 116.397827\n" +
            "    },\n" +
            "    \"distance\" : 0.0\n" +
            "  } ]\n" +
            "}";

    public static void main(String[] args) {
        JSONObject object = JSONObject.parseObject(str);
        JSONArray vo = JSONArray.parseArray(object.getString("data"));
        List<OrderResponseVo> list = new ArrayList<>();
        vo.stream().forEach(e -> {
            OrderResponseVo v = JSONObject.parseObject(e.toString(), OrderResponseVo.class);
            list.add(v);
        });

        list.stream().map( e -> {
            double distance = LocationUtil.distance(
                    116.40717,39.90469,
                    e.getLocation().getDouble("latitude"),
                    e.getLocation().getDouble("longitude"));
            e.setDistance(distance);
            return e;
        }).collect(Collectors.toList());

        System.out.println(list.get(0).getLocation());
    }


}
