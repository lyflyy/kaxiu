# kaxiu

### 卡修项目交流群
![1](https://phonics-1253604546.cos.ap-beijing.myqcloud.com/WechatIMG83.jpeg)

### 卡修本是一个为高速公路上的汽车提供维修服务的产品，分为卡修服务器端，卡修用户端小程序，卡修维修端小程序，暂时还没有对管理端进行开发。
### 开发文档
- [脑图](/doc/卡修脑图文档.xmind)
- [开发表梳理](/doc/表梳理.png)
- [小程序开发文档](小程序开发文档.md)
- [sql脚本](/sql/kaxiu-20190813.sql)
- 本项目包含 服务器端项目 kaxiu-server、用户端小程序项目 kaxiu-consumer,以及维修端小程序 kaxiu-tenant

#### 服务流程
> ##### 普通用户
    1. 普通用户登陆小程序，微信授权获取用户信息，地理位置信息
    2. 选择维修类型
    3. 拍摄故障现场图片
    4. 提交订单
> ##### 维修人员
    1. 登陆并授权信息、位置
    2. 认证
    3. 管理员认证（现直接调用接口进行认证）详见（TestController）
    4. 认证成功等待订单(现直接请求接口模拟用户支付)详见（TestController）
    5. 接单
    6. 拍摄维修图片
    7. 完成维修，系统对用户付款进行计算，一定比例会被平台扣除，剩余会加到维修人员账户（提现现未开发）

### 开发框架
- springboot
- [mybatisplus](https://mp.baomidou.com/)
- 权限认证 shiro + jwt
- 消息队列 mqtt
- 小程序 [weixin-java-tools](https://gitee.com/binary/weixin-java-tools)

### 暂未完成的功能
1. 支付
2. 手机短信
3. 根据不同的条件推送不同的订单，现在推送给所有的维修人员

### 服务启动
1. 安装并配置 mysql
2. 安装配置redis
3. 安装[apache activemq artemis](http://activemq.apache.org/components/artemis/download/)
    - ```
        // 接下来会输入账号密码等信息，账号密码的配置要放在项目的配置文件中
        artemis create mybroker
        // 启动包含mqtt协议的activemq服务
        mybroker/bin/artemis run
      ```
4. 安装配置[openresty](https://openresty.org/cn/)
    - 需要修改配置文件转发sokect请求与小程序的请求
    ```
    server { 
         access_log  logs/access.log;
         listen 443;
         ssl on;
         ssl_certificate /Users/liyang/work/ssl/ca.crt;
         ssl_certificate_key /Users/liyang/work/ssl/ca.key;
         ssl_session_timeout 5m;
         ssl_session_cache shared:SSL:50m;
         ssl_protocols SSLv3 SSLv2 TLSv1 TLSv1.1 TLSv1.2;
         ssl_ciphers ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP;
         underscores_in_headers on;#开启自定义头信息的下划线
         location /wss {
                 proxy_pass http://127.0.0.1:1883;#代理到上面的地址去
                 proxy_http_version 1.1;
                 proxy_set_header Upgrade $http_upgrade;
                 proxy_set_header Connection "Upgrade";
                 proxy_redirect off;
                 #重要配置，解决小程序连接 Sec-WebSocket-Protocol错误提示
                 proxy_set_header Sec-WebSocket-Protocol mqtt;
                 # more_clear_headers 'Sec-WebSocket-Protocol';
         }
         #https协议转发 小程序里面要访问的链接
         location /{
                 proxy_pass http://127.0.0.1:8081;#代理到原有的http的地址去
                 proxy_set_header   X-Real-IP        $remote_addr;
                 proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
                 add_header Access-Control-Allow-Origin *;#跨域访问设置
         }
     }
    ```
5. 启动服务，访问小程序



###项目截图

用户端
| ![1](/doc/img/用户端/1.png) | ![2](/doc/img/用户端/2.png) | ![3](/doc/img/用户端/3.png) | ![4](/doc/img/用户端/4.png) |
| --------------------------- | --------------------------- | --------------------------- | --------------------------- |
| ![5](/doc/img/用户端/5.png) | ![6](/doc/img/用户端/6.png) | ![7](/doc/img/用户端/7.png) | ![8](/doc/img/用户端/8.png) |


维修端
| ![1](/doc/img/维修端/1.png) | ![2](/doc/img/维修端/2.png) | ![2](/doc/img/维修端/3.png) | ![2](/doc/img/维修端/4.png) | ![2](/doc/img/维修端/5.png)  |
| --------------------------- | --------------------------- | --------------------------- | --------------------------- | ---------------------------- |
| ![2](/doc/img/维修端/6.png) | ![2](/doc/img/维修端/7.png) | ![2](/doc/img/维修端/8.png) | ![2](/doc/img/维修端/9.png) | ![2](/doc/img/维修端/10.png) |




