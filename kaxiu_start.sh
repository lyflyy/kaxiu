
#!/bin/bash

# 打开docker
open /Applications/Docker.app

# 打开idea
open /Applications/IntelliJ\ IDEA.app

# 打开hbuder
open /Applications/HBuilderX.app

# 打开chrome
open /Applications/Google\ Chrome.app

# 开启activemq服务
cd /Users/liyang/Applications/apache-artemis-2.9.0/bin/mybroker/bin/
./artemis-service start

# 打开openresty
cd /Users/liyang/work
./start.sh

# 打开docker服务 mysql、redis
sleep 30
docker start c4d962b85678
docker start 0be64017e380

exit
